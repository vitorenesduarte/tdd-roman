# tdd-roman

__Learning TDD while__:

* converting Roman strings to numbers
* converting numbers to Roman strings

__See__:

* [Roman numerals - Wikipedia](https://en.wikipedia.org/wiki/Roman_numerals)