package com.tdd.roman;

import java.util.TreeSet;

/**
 *
 * @author Vitor Enes <vitorenesduarte at gmail.com>
 */
public class RomanUtil {

    public static int firstDigit(int number) {
        while (number > 9) {
            number /= 10;
        }

        return number;
    }

    public static int countDigits(int number) {
        return (int) (Math.log10(number) + 1);
    }

    public static int absolute(int number) {
        // 42 -> 40
        // 1234 -> 1000
        return firstDigit(number) * (int) Math.pow(10, countDigits(number) - 1);
    }

    public static int closestRomanValue(int number) {
        // if number is 3, there are two closest values, 1 and 5
        // we want the lowest
        int diff = Integer.MAX_VALUE;
        int closest = 0;
        TreeSet<Integer> values = new TreeSet<>();

        for (Integer romanValue : RomanValues.values()) {
            int newDiff = Math.abs(romanValue - number);

            if (newDiff <= diff) {
                if (newDiff < diff) {
                    diff = newDiff;
                    closest = romanValue;
                } else { // I already found one value with the same diff
                    values.add(closest);
                    values.add(romanValue);
                    break;
                }
            }
        }

        if (values.isEmpty()) {
            values.add(closest);
        }

        return values.first();
    }

    public static StringBuilder getRomanValueOfAbsolute(int absolute) {
        int closest = closestRomanValue(absolute);
        Character roman = RomanValues.value(closest);
        Integer romanValue = RomanValues.value(roman);
        StringBuilder sb = new StringBuilder();

        if (romanValue == absolute) {
            sb.append(roman);
        } else if (romanValue < absolute) {
            while (absolute > 0) {
                sb.append(roman);
                absolute -= romanValue;
            }
        } else {
            Integer diff = romanValue - absolute;
            sb.append(RomanValues.value(diff));
            sb.append(roman);
        }

        return sb;
    }
}
