package com.tdd.roman;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Vitor Enes <vitorenesduarte at gmail.com>
 */
public class Roman {

    // http://www.factmonster.com/ipka/A0769547.html
    private static final Pattern ROMAN = Pattern.compile("^M{0,3}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$");

    public static int from(String from) throws InvalidRomanException {
        if (!valid(from)) {
            throw new InvalidRomanException();
        }

        char[] fromArray = from.toCharArray();
        int[] values = new int[fromArray.length];

        for (int i = 0; i < fromArray.length; i++) {
            values[i] = RomanValues.value(fromArray[i]);
        }

        int result = 0;

        for (int i = 0; i < values.length; i++) {
            int value = values[i];
            int signal = 1;

            if (i + 1 < values.length) {
                int nextValue = values[i + 1];
                if (nextValue > value) {
                    signal = -1;
                }
            }

            result += value * signal;
        }

        return result;
    }

    public static String to(int to) throws InvalidRomanException {
        if (!valid(to)) {
            throw new InvalidRomanException();
        }

        StringBuilder sb = new StringBuilder();
        while (to > 0) {
            int absolute = RomanUtil.absolute(to);
            sb.append(RomanUtil.getRomanValueOfAbsolute(absolute));

            to = to - absolute;
        }

        return sb.toString();
    }

    private static boolean valid(String value) {
        Matcher matcher = ROMAN.matcher(value);
        return matcher.matches();
    }

    private static boolean valid(int value) {
        return value < 4000 && value > 0;
    }
}
