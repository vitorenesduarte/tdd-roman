package com.tdd.roman;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Vitor Enes <vitorenesduarte at gmail.com>
 */
public class RomanValues {

    private static final Map<Character, Integer> FROM = new HashMap<>();
    private static final Map<Integer, Character> TO = new HashMap<>();

    static {
        FROM.put('I', 1);
        FROM.put('V', 5);
        FROM.put('X', 10);
        FROM.put('L', 50);
        FROM.put('C', 100);
        FROM.put('D', 500);
        FROM.put('M', 1000);

        TO.put(1, 'I');
        TO.put(5, 'V');
        TO.put(10, 'X');
        TO.put(50, 'L');
        TO.put(100, 'C');
        TO.put(500, 'D');
        TO.put(1000, 'M');
    }

    public static Integer value(Character key) {
        return FROM.get(key);
    }

    public static Character value(Integer key) {
        return TO.get(key);
    }

    public static Set<Integer> values() {
        return TO.keySet();
    }
}
