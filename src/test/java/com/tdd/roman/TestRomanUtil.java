package com.tdd.roman;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author Vitor Enes <vitorenesduarte at gmail.com>
 */
public class TestRomanUtil {

    @Test
    public void testFirstDigit() {
        assertEquals(9, RomanUtil.firstDigit(9));
        assertEquals(4, RomanUtil.firstDigit(42));
        assertEquals(1, RomanUtil.firstDigit(123));
        assertEquals(9, RomanUtil.firstDigit(9876));
    }

    @Test
    public void testCountDigits() {
        assertEquals(1, RomanUtil.countDigits(9));
        assertEquals(2, RomanUtil.countDigits(42));
        assertEquals(3, RomanUtil.countDigits(123));
        assertEquals(4, RomanUtil.countDigits(9876));
    }

    @Test
    public void testAbsolute() {
        assertEquals(9, RomanUtil.absolute(9));
        assertEquals(100, RomanUtil.absolute(123));
        assertEquals(40, RomanUtil.absolute(42));
        assertEquals(9000, RomanUtil.absolute(9876));
    }

    @Test
    public void testClosestRomanValues() {
        assertEquals(1, RomanUtil.closestRomanValue(1));
        assertEquals(1, RomanUtil.closestRomanValue(3));
        assertEquals(10, RomanUtil.closestRomanValue(9));
        assertEquals(10, RomanUtil.closestRomanValue(30));
        assertEquals(100, RomanUtil.closestRomanValue(123));
    }

}
