package com.tdd.roman;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Test;

/**
 *
 * @author Vitor Enes <vitorenesduarte at gmail.com>
 */
public class TestRoman {

    @Test
    public void testFromRomanSingleLetter() {
        try {
            assertEquals(1, Roman.from("I"));
            assertEquals(5, Roman.from("V"));
            assertEquals(10, Roman.from("X"));
            assertEquals(50, Roman.from("L"));
            assertEquals(100, Roman.from("C"));
            assertEquals(500, Roman.from("D"));
            assertEquals(1000, Roman.from("M"));
        } catch (InvalidRomanException ex) {
            fail(ex.getMessage());
        }
    }

    @Test
    public void testFromRomanSeveralLetters() {
        try {
            assertEquals(2, Roman.from("II"));
            assertEquals(3, Roman.from("III"));
            assertEquals(4, Roman.from("IV"));
            assertEquals(9, Roman.from("IX"));
            assertEquals(39, Roman.from("XXXIX"));
            assertEquals(40, Roman.from("XL"));
            assertEquals(339, Roman.from("CCCXXXIX"));
            assertEquals(400, Roman.from("CD"));
            assertEquals(1904, Roman.from("MCMIV"));
            assertEquals(1954, Roman.from("MCMLIV"));
            assertEquals(1990, Roman.from("MCMXC"));
            assertEquals(2014, Roman.from("MMXIV"));
        } catch (InvalidRomanException ex) {
            fail(ex.getMessage());
        }
    }

    @Test(expected = InvalidRomanException.class)
    public void testFromRomanInvalid() throws InvalidRomanException {
        Roman.from("MMMM");
    }

    @Test
    public void testToRomanSingleLetter() {
        try {
            assertEquals("I", Roman.to(1));
            assertEquals("V", Roman.to(5));
            assertEquals("X", Roman.to(10));
            assertEquals("L", Roman.to(50));
            assertEquals("C", Roman.to(100));
            assertEquals("D", Roman.to(500));
            assertEquals("M", Roman.to(1000));
        } catch (InvalidRomanException ex) {
            fail(ex.getMessage());
        }
    }

    @Test
    public void testToRomanSeveralLetters() {
        try {
            assertEquals("II", Roman.to(2));
            assertEquals("III", Roman.to(3));
            assertEquals("IV", Roman.to(4));
            assertEquals("IX", Roman.to(9));
            assertEquals("XXXIX", Roman.to(39));
            assertEquals("XL", Roman.to(40));
            assertEquals("CCCXXXIX", Roman.to(339));
            assertEquals("CD", Roman.to(400));
            assertEquals("MCMIV", Roman.to(1904));
            assertEquals("MCMLIV", Roman.to(1954));
            assertEquals("MCMXC", Roman.to(1990));
            assertEquals("MMXIV", Roman.to(2014));
        } catch (InvalidRomanException ex) {
            fail(ex.getMessage());
        }
    }

    @Test(expected = InvalidRomanException.class)
    public void testToRomanInvalid() throws InvalidRomanException {
        Roman.to(4000);
    }
}
